
#include <iostream>
#include <cmath>
#include <ctime>
#include <vector>
#include "../../rapidjson/document.h"
#include "../../rapidjson/writer.h"
#include "../../rapidjson/stringbuffer.h"

using namespace std;
using namespace rapidjson;

unsigned char ** allocateMatrix( int rSz, int cSz ) {

  unsigned char **ptM = NULL;
  int ctR, ctC;


  /* Allocate pointers to all rows. */
  ptM = new (std::nothrow)  unsigned char * [ rSz ];
  if ( ptM == 0 )
    return NULL;

  /* Allocate each row individually */
  for ( ctR = 0; ctR < rSz; ctR++ ) {
    *(ptM + ctR ) = new (std::nothrow)  unsigned char [ cSz ];
    if ( *(ptM + ctR )  == 0 )
      return NULL;

          /* Fill up this new row of the matrix. */
    for ( ctC = 0; ctC < cSz; ctC++ )
      ptM[ ctR ][ ctC ] = ctR*cSz + ctC + 1;
  }

  return ptM;
}

void deleteMatrix( unsigned char ** dptM, int rSz ) {
  int ctR;

  /* Delete each row previously allocated. */
  for ( ctR = 0; ctR < rSz; ctR++ )
    if ( *(dptM + ctR ) != NULL )
      delete [] *(dptM + ctR) ;

  /* Safely deletes the array of pointers now. */
  if  ( dptM != NULL )
    delete [] dptM;

}


struct timespec My_diff(struct timespec start, struct timespec end) {
    struct timespec temp;
    if ((end.tv_nsec - start.tv_nsec) < 0) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}

/// Establish the number of bins
const int HIST_SIZE = 256;
unsigned int hist[HIST_SIZE];

int main(int argc, char **argv) {

	struct timespec timeInit, timeEnd;

    int n=atoi(argv[1]);
    int m=atoi(argv[2]);

    unsigned char** image = allocateMatrix(n,m);

    time_t t;
    srand((unsigned)time(&t));        
    for(unsigned int i = 0; i < n; i++){
        for(unsigned int j = 0; j < m; j++){
            image[i][j] = rand() % 256;
        }
    }

    if (clock_gettime(CLOCK_REALTIME, &timeInit)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }

    for(unsigned int i = 0; i < n; i++){
        for(unsigned int j = 0; j < m; j++){
            unsigned char index = image[i][j];
            hist[index]++;
        }
    }

    if (clock_gettime(CLOCK_REALTIME, &timeEnd)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }

    //constante equivalente a segundos em nanosegundos
    double ONE_SECOND_IN_NANOSECONDS = 1000000000;
    //calculando a  diferença entre os dois tempos
    struct timespec timeDiff;
    timeDiff = My_diff(timeInit, timeEnd);
    //somando o tempo em segundo mais o tempo restante em nanosegundos.
    double processTime = (timeDiff.tv_sec + (double) timeDiff.tv_nsec / ONE_SECOND_IN_NANOSECONDS);

    //-----------JSON-----------
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("time");
    //writer.SetMaxDecimalPlaces(10);
    writer.Double(processTime);
    writer.Key("hist");
    writer.StartArray();
    for (unsigned i = 0; i < HIST_SIZE; i++)
        writer.Uint(hist[i]);
    writer.EndArray();
    writer.EndObject();

    cout << s.GetString() << endl;

    deleteMatrix(image,n);

    /*
    const char* json = "{\"time\":0.0}";
    rapidjson::Document d;
    d.Parse(json);

    rapidjson::Value& s = d["time"];
    s.SetDouble(processTime);       

	vector<int> vecHist( begin(hist), end(hist) ) ;
	d.SetArray();

	rapidjson::Document::AllocatorType& allocator = d.GetAllocator();
	for (int i = 0; i < vecHist.size(); i++)
	{
   		d.PushBack(vecHist.at(i),allocator);
	}

    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    d.Accept(writer);

    printf("%s\n",buffer.GetString());
*/

	return 0;
}

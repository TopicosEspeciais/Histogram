
//# Flags used to disable display capablities of CImg
//NODISPLAY_CFLAGS = -Dcimg_display=0
//g++ -o histogram_seq histogram_seq.cpp -Dcimg_display=0

#include "../../CImg_latest/CImg-2.2.2/CImg.h"
#include  <iostream>
#include <jpeglib.h>
#include <jerror.h>
#include <math.h>
#include <time.h>
#include <vector>
#include "../../rapidjson/document.h"
#include "../../rapidjson/writer.h"
#include "../../rapidjson/stringbuffer.h"

using namespace cimg_library;
using namespace std;


/*
 *Função responsável por calcular a diferença dos dois tempos
 *recebe como paramentro dois timespec e calcula a diferença entre eles
 */
struct timespec My_diff(struct timespec start, struct timespec end) {
    struct timespec temp;
    //Caso a diferença em nanosegundos for negativo, então deve-se somar um segundo
    //no campo de nanosegundos e pedir emprestado um segundo no campo de segundos.
    //Como se fosse uma subtração onde voce pede emprestado '1' do proximo numero mais
    //importante.
    if ((end.tv_nsec - start.tv_nsec) < 0) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}

/// Establish the number of bins
const int HIST_SIZE = 256;
int hist[HIST_SIZE];

int main(int argc, char **argv) {

	struct timespec timeInit, timeEnd;

	CImg<unsigned char> image(argv[1]),
	grayWeight(image.width(), image.height(), 1, 1, 0),
	imgR(image.width(), image.height(), 1, 3, 0),
	imgG(image.width(), image.height(), 1, 3, 0),
	imgB(image.width(), image.height(), 1, 3, 0);
    

    if (clock_gettime(CLOCK_REALTIME, &timeInit)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }


	for(int x = 0; x < image.width(); x++)
	{
		for (int y = 0; y < image.height(); y++)
		{
    		// Separation of channels
			int R = (int)image(x,y,0,0);
			int G = (int)image(x,y,0,1);
			int B = (int)image(x,y,0,2);
        	// Real weighted addition of channels for gray
			int grayValueWeight = (int)(0.299*R + 0.587*G + 0.114*B);
			// Index of the histogram vector
			int index = grayValueWeight;
			hist[index]++;
		}
	}

    if (clock_gettime(CLOCK_REALTIME, &timeEnd)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }

    //constante equivalente a segundos em nanosegundos
    double ONE_SECOND_IN_NANOSECONDS = 1000000000;
    //calculando a  diferença entre os dois tempos
    struct timespec timeDiff;
    timeDiff = My_diff(timeInit, timeEnd);
    //somando o tempo em segundo mais o tempo restante em nanosegundos.
    double processTime = (timeDiff.tv_sec + (double) timeDiff.tv_nsec / ONE_SECOND_IN_NANOSECONDS);

    //-----------JSON-----------

    const char* json = "{\"time\":0.0}";
    rapidjson::Document d;
    d.Parse(json);

    rapidjson::Value& s = d["time"];
    s.SetDouble(processTime);       

	vector<int> vecHist( begin(hist), end(hist) ) ;
	d.SetArray();

	rapidjson::Document::AllocatorType& allocator = d.GetAllocator();
	for (int i = 0; i < vecHist.size(); i++)
	{
   		d.PushBack(vecHist.at(i),allocator);
	}

    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    d.Accept(writer);

    printf("%s\n",buffer.GetString());

	return 0;
}

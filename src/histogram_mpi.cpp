
#include <iostream>
#include <cmath>
#include <ctime>
#include <vector>
#include <omp.h>
#include "mpi.h"
#include "../../rapidjson/document.h"
#include "../../rapidjson/writer.h"
#include "../../rapidjson/stringbuffer.h"

using namespace std;
using namespace rapidjson;

unsigned char ** allocateMatrix( int rSz, int cSz ) {

  unsigned char **ptM = NULL;
  int ctR, ctC;


  /* Allocate pointers to all rows. */
  ptM = new (std::nothrow)  unsigned char * [ rSz ];
  if ( ptM == 0 )
    return NULL;

  /* Allocate each row individually */
  for ( ctR = 0; ctR < rSz; ctR++ ) {
    *(ptM + ctR ) = new (std::nothrow)  unsigned char [ cSz ];
    if ( *(ptM + ctR )  == 0 )
      return NULL;

          /* Fill up this new row of the matrix. */
    for ( ctC = 0; ctC < cSz; ctC++ )
      ptM[ ctR ][ ctC ] = 0;
  }

  return ptM;
}

void deleteMatrix( unsigned char ** dptM, int rSz ) {
  int ctR;

  /* Delete each row previously allocated. */
  for ( ctR = 0; ctR < rSz; ctR++ )
    if ( *(dptM + ctR ) != NULL )
      delete [] *(dptM + ctR) ;

  /* Safely deletes the array of pointers now. */
  if  ( dptM != NULL )
    delete [] dptM;

}

/// Establish the number of bins
const int HIST_SIZE = 256;
unsigned long int hist[HIST_SIZE];

int main(int argc, char **argv) {

    char hostname[MPI_MAX_PROCESSOR_NAME];

    double pi, initial_time=0, final_time=0;
    int numTasks, rank, rc,len;

    int n=atoi(argv[1]);
    int m=atoi(argv[2]);    

    unsigned char** image = allocateMatrix(n,m);

    time_t t;
    srand((unsigned)time(&t));
    //TODO alternate to rand_r
    #pragma omp parallel for collapse(2)
    for(unsigned int i = 0; i < n; i++){
        for(unsigned int j = 0; j < m; j++){
            image[i][j] =  rand() % 256;
        }
    }

    rc = MPI_Init(&argc, &argv);

  if (rc != MPI_SUCCESS) {
        printf("Error starting MPI program.\n");
        MPI_Abort(MPI_COMM_WORLD, rc);
    }

    MPI_Get_processor_name(hostname, &len);

    MPI_Comm_size(MPI_COMM_WORLD, &numTasks);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    initial_time = MPI_Wtime();

    //printf("rank %d hostname %s initial_time %f \n",rank,hostname,initial_time);

  unsigned int localRows = n / numTasks;

  

    #pragma omp parallel
   {
       // if( omp_get_thread_num() == 0 ){
           // int nthreads = omp_get_num_threads();
           // printf("rank %d hostname %s start i %d finish %d numTasks %d numbTHreads %d\n",rank,hostname,rank*localRows,(rank+1)*localRows, numTasks,nthreads);
       // }

        #pragma omp for reduction(+:hist[:HIST_SIZE]) collapse(2)
        for(unsigned int i = rank*localRows; i < (rank+1)*localRows; i++){
            for(unsigned int j = 0; j < m; j++){
             // printf("rank %d hostname %s threadNum %d i %d j %d index %d\n",rank,hostname,omp_get_thread_num(),i,j,image[i][j]);
                unsigned char index = image[i][j];
                hist[index]++;
            }
        }
   }

  unsigned long int global_hist[HIST_SIZE];
  MPI_Reduce(&hist, &global_hist, HIST_SIZE, MPI_UNSIGNED_LONG, MPI_SUM, 0, MPI_COMM_WORLD);

    final_time = MPI_Wtime();

    //-----------JSON-----------
    if(rank == 0){

        int sum = 0;
        for (unsigned i = 0; i < HIST_SIZE; i++)
            sum+=global_hist[i];

        StringBuffer s;
        Writer<StringBuffer> writer(s);

        writer.StartObject();
        writer.Key("time");
        //writer.SetMaxDecimalPlaces(10);
        writer.Double(final_time - initial_time);
        writer.Key("sum");
        writer.Double(sum);
        writer.Key("hist");
        writer.StartArray();
        for (unsigned i = 0; i < HIST_SIZE; i++)
            writer.Uint(global_hist[i]);
        writer.EndArray();
        writer.EndObject();

        cout << s.GetString() << endl;
    }

    deleteMatrix(image,n);

    /*
    const char* json = "{\"time\":0.0}";
    rapidjson::Document d;
    d.Parse(json);

    rapidjson::Value& s = d["time"];
    s.SetDouble(processTime);

  vector<int> vecHist( begin(hist), end(hist) ) ;
  d.SetArray();

  rapidjson::Document::AllocatorType& allocator = d.GetAllocator();
  for (int i = 0; i < vecHist.size(); i++)
  {
      d.PushBack(vecHist.at(i),allocator);
  }

    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    d.Accept(writer);

    printf("%s\n",buffer.GetString());
*/
    MPI_Finalize();
  return 0;
}

#! /usr/bin/env python3
import subprocess
import os
from collections import namedtuple


Time = namedtuple("Time", "d h m s")

t = Time(0,0,5,0)

nodes = 1
cpus_per_task = 6
n_tasks_per_node = 1
os.environ["OMP_NUM_THREADS"] = str(cpus_per_task)
#subprocess.check_output(['export OMP_NUM_THREADS='+str(cpus_per_task)])

for repetion in range(1,2):
	rc = subprocess.call(
		['sbatch','--job-name=Hist_OMP','--partition=test','--hint=compute_bound',
		'--output=../results/openmp/%j_'+str(nodes)+'_'+str(n_tasks_per_node)+'_'+str(cpus_per_task)+str(repetion)+'.json',
		'--error=../error/openmp/%j_'+str(nodes)+'_'+str(n_tasks_per_node)+'_'+str(cpus_per_task)+str(repetion)+'.err',
		'--nodes='+str(nodes),'--ntasks-per-node='+str(n_tasks_per_node),'--cpus-per-task='+str(cpus_per_task),
		'--time='+str(t.d)+'-'+str(t.h)+':'+str(t.m)+':'+str(t.s),'--exclusive',
		'../sh/openmp.sh','/home/damcoutinho2/Projects/Histogram/lena.jpg'])

# '--mail-user=ilovebasketball.d@gmail.com','--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90'
#subprocess.check_output -- check the command
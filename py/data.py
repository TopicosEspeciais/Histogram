#! /usr/bin/env python3
import json
import os
import re
from statistics import median
from operator import itemgetter
from pprint import pprint

# CONSTANTS
CONST_N = 'n'
CONST_M = 'm'
CONST_MPI = 'mpi'
CONST_SEQ = 'seq'
CONST_ID = 'id'
CONST_NODES = 'nodes'
CONST_NTASKS = 'n_tasks_per_node'
CONST_CPUTASKS = 'cpus_per_task'
CONST_REP = 'repetition'

CONST_HOME = os.environ['HOME']
CONST_PATH_RESULTS = '/Projects/Histogram/results/'
CONST_PATH_ERRORS = '/Projects/Histogram/error/'

CONST_PATH_RESULTS_MPI = CONST_HOME + CONST_PATH_RESULTS + CONST_MPI
CONST_PATH_RESULTS_SEQ = CONST_HOME + CONST_PATH_RESULTS + CONST_SEQ

CONST_PATH_ERROS_MPI = CONST_HOME + CONST_PATH_ERRORS + CONST_MPI
CONST_PATH_ERROS_SEQ = CONST_HOME + CONST_PATH_ERRORS + CONST_SEQ


class ParseResults:
    lmpi = []
    lseq = []
    errors = dict(mpi=dict(), seq=dict())

    def parse_mpi(self):
        for filename in os.listdir(CONST_PATH_RESULTS_MPI):
            with open(CONST_PATH_RESULTS_MPI + '/' + filename) as f:

                info = re.split(r'[_.]', filename)

                try:
                    data = json.load(f)
                except ValueError:
                    data = dict(time=-1, sum=-1, hist=[])
                    self.errors[CONST_MPI][info[0]] = []

                data[CONST_ID] = info[0]
                data[CONST_NODES] = info[1]
                data[CONST_NTASKS] = info[2]
                data[CONST_CPUTASKS] = info[3]
                data[CONST_N] = info[4]
                data[CONST_M] = info[5]
                data[CONST_REP] = info[6]
                self.lmpi.append(data)
                self.lmpi = sorted(self.lmpi, key=itemgetter(CONST_ID))

    def parse_seq(self):
        for filename in os.listdir(CONST_PATH_RESULTS_SEQ):
            with open(CONST_PATH_RESULTS_SEQ + '/' + filename) as f:

                info = re.split(r'[_.]', filename)

                try:
                    data = json.load(f)
                except ValueError:
                    data = {"time": -1, "hist": []}
                    self.errors[CONST_SEQ][info[0]] = []

                data[CONST_ID] = info[0]
                data[CONST_N] = info[1]
                data[CONST_M] = info[2]
                data[CONST_REP] = info[3]
                self.lseq.append(data)
                self.lseq = sorted(self.lseq, key=itemgetter(CONST_ID))

    def check_mpi_errors(self):
        for filename in os.listdir(CONST_PATH_ERROS_MPI):
            with open(CONST_PATH_ERROS_MPI + '/' + filename) as f:
                txt = f.read()
                if txt != '':
                    info = re.split('_', filename)
                    self.errors[CONST_MPI][info[0]] = [CONST_PATH_ERROS_MPI + '/' + filename, txt]

    def check_seq_errors(self):
        for filename in os.listdir(CONST_PATH_ERROS_SEQ):
            with open(CONST_PATH_ERROS_SEQ + '/' + filename) as f:
                txt = f.read()
                if txt != '':
                    info = re.split('_', filename)
                    self.errors[CONST_SEQ][info[0]] = [CONST_PATH_ERROS_SEQ + '/' + filename, txt]


class ParallelStats:
    lmedian_by_key = dict()

    def median_by_key(self, key, data):
        if key not in self.lmedian_by_key:
            self.lmedian_by_key.update({key: []})

        for i in range(0, len(data), 3):
            m = median([data[i][key], data[i + 1][key], data[i + 2][key]])
            self.lmedian_by_key[key].append(m)


ps = ParseResults()
ps.check_seq_errors()
ps.check_mpi_errors()

pprint(ps.errors)

for seq in ps.lseq:
    print(str(seq["id"])+" "+str(seq["n"])+" "+str(seq["m"])+" "+str(seq["time"]))

#! /usr/bin/env python3
import subprocess
from collections import namedtuple
import os

#os.environ["TMPDIR"] = '/tmp/scratch/damcoutinho2'

Time = namedtuple("Time", "d h m s")

t = Time(0, 0, 29, 0)
n_tasks_per_node = 1
min = 13  # 13
max = 19 # 19
rMax = 3 #3

for nodes in [1]:
    for cpus_per_task in [2,4,8,16]:
        os.environ["OMP_NUM_THREADS"] = str(cpus_per_task)
        for i in range(min,max):
            n = 2 ** i
            for j in range(min, max):
                m = 2 ** j
                for repetion in range(1, rMax + 1):
                    rc = subprocess.call(
                    ['sbatch', '--job-name=HMPI', '--partition=test',
                    '--output=../results/mpi/%j_' + str(nodes) + '_' + str(n_tasks_per_node) + '_' + str(
                                 cpus_per_task) + '_' + str(n)+'_'+str(m)+'_'+str(repetion) + '.json',
                    '--error=../error/mpi/%j_'  + str(nodes) + '_' + str(n_tasks_per_node) + '_' + str(
                                 cpus_per_task) + '_'+ str(n)+'_'+str(m)+'_'+str(repetion) + '.err',
                    '--nodes=' + str(nodes), '--ntasks-per-node=' + str(n_tasks_per_node),
                    '--cpus-per-task=' + str(cpus_per_task),'--hint=compute_bound',
                    '--time=' + str(t.d) + '-' + str(t.h) + ':' + str(t.m) + ':' + str(t.s), '--exclusive', '--mem=80G',
                    '--mail-user=ilovebasketball.d@gmail.com', '--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90',
                    '../sh/mpi.sh', str(n), str(m)])

# '--mail-user=ilovebasketball.d@gmail.com','--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90'
# subprocess.check_output -- check the command

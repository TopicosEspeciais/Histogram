############################# Makefile ##########################
CODE = src/
BIN  = bin/
RESU = results/
ERR  = error/


all: openmp seq mpi

openmp:	$(CODE)histogram_openmp.cpp
			g++ $(CODE)histogram_openmp.cpp -fopenmp  -o $(BIN)histogram_openmp
seq:	$(CODE)histogram_seq.cpp
			g++ $(CODE)histogram_seq.cpp -lrt -lm  -o $(BIN)histogram_seq 
mpi:	$(CODE)histogram_mpi.cpp
			mpicxx $(CODE)histogram_mpi.cpp -fopenmp  -o $(BIN)histogram_mpi

cbin:
			rm -rf  $(BIN)*
cseq: 
			rm -rf $(RESU)/seq/* $(RESU)/parallel/*
cerr: 
			rm -rf $(ERR)/seq/* $(ERR)/parallel/*
cfiles: cseq cerr	
call: cbin cfiles


#!/bin/bash
#SBATCH --job-name=OMPMPI_R4
#SBATCH --output=../slurm_outputs/slurm-Rev4-nodes-2-rows-16-%j.out
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=1
#SBATCH --hint=compute_bound
#SBATCH --partition=test
#SBATCH --time=0-0:2

export OMP_NUM_THREADS=2

srun ../histogram_MPI_OpenMP_Rev4 0 10 10 2 1

#!/bin/bash
#SBATCH --job-name=OMPMPI_R4
#SBATCH --output=../slurm_outputs/slurm-Rev4-nodes-2-rows-18-%j.out
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=1
#SBATCH --hint=compute_bound
#SBATCH --mem=120G
#SBATCH --exclusive
#SBATCH --partition=test
#SBATCH --time=0-0:29

export OMP_NUM_THREADS=32

srun ../histogram_MPI_OpenMP_Rev4 0 18 16 2 1
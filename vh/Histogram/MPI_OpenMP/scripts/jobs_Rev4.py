import subprocess

for i in range(5):
	for nodes in [2, 4]:
		for rows in [16, 17, 18, 19, 20]:
			name = "job_OMP_MPI_test_Rev4_nodes_" + str(nodes) + "_rows_" + str(rows) + ".sh"
			rc = subprocess.run(["sbatch", name])

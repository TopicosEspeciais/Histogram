#!/bin/bash
#SBATCH --job-name=OMP_R3
#SBATCH --output=../slurm_outputs/slurm-Rev3-threads-32-rows-17-%j.out
#SBATCH --error=../slurm_outputs/slurm-Rev3-threads-32-rows-17-%j.err
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=32
#SBATCH --hint=compute_bound
#SBATCH --mem=120G
#SBATCH --exclusive
#SBATCH --partition=test
#SBATCH --time=0-0:29

export OMP_NUM_THREADS=32

srun ../histogram_OpenMP_Rev3 0 17 16 1 1
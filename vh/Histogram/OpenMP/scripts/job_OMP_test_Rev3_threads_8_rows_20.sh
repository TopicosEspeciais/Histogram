#!/bin/bash
#SBATCH --job-name=OMP_R3
#SBATCH --output=../slurm_outputs/slurm-Rev3-threads-8-rows-20-%j.out
#SBATCH --error=../slurm_outputs/slurm-Rev3-threads-8-rows-20-%j.err
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=8
#SBATCH --hint=compute_bound
#SBATCH --mem=120G
#SBATCH --exclusive
#SBATCH --partition=test
#SBATCH --time=0-0:29

export OMP_NUM_THREADS=8

srun ../histogram_OpenMP_Rev3 0 20 16 1 1
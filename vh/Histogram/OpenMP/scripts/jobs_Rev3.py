import subprocess

for i in range(5):
	for threads in [2, 4, 8, 16, 32]:
		for rows in [16, 17, 18, 19, 20]:
			name = "job_OMP_test_Rev3_threads_" + str(threads) + "_rows_" + str(rows) + ".sh"
			rc = subprocess.run(["sbatch", name])

#!/bin/bash
#SBATCH --job-name=OMP_R3
#SBATCH --output=../slurm_outputs/slurm-Rev3-threads-32-rows-20-%j.out
#SBATCH --error=../slurm_outputs/slurm-Rev3-threads-32-rows-20-%j.err
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=2
#SBATCH --partition=test
#SBATCH --time=0-0:3

export OMP_NUM_THREADS=2

srun ../histogram_OpenMP_Rev3 0 13 13 1 1
